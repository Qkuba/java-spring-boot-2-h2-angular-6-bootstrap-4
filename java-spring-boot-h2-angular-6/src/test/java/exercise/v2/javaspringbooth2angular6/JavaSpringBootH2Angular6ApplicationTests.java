package exercise.v2.javaspringbooth2angular6;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JavaSpringBootH2Angular6ApplicationTests {

	@Test
	public void contextLoads() {
	}

}
