insert into campaign (name, keywords, bid_amount, fund, status, town, radius)
values('Segregation of garbage','recycling, segregation',2000, 6000, true, 'Katowice', 100);

insert into campaign (name, keywords, bid_amount, fund, status, town, radius)
values('Free ride by public transport','mpk, free ride',3000, 90000, false, 'Kraków', 20);

insert into campaign (name, keywords, bid_amount, fund, status, town, radius)
values('"Męskie granie" concerts','concert, event',7000, 12000, true, 'Wrocław', 0);

insert into resource (towns, keywordss, min_amount, saldo)
values('Kraków','recycling',1000,50000);

insert into resource (towns, keywordss, min_amount, saldo)
values('Katowice','segregation',null,null);

insert into resource (towns, keywordss, min_amount, saldo)
values('Warszawa','mpk',null,null);

insert into resource (towns, keywordss, min_amount, saldo)
values('Wrocław','free ride',null,null);

insert into resource (towns, keywordss, min_amount, saldo)
values('Poznań','concert',null,null);

insert into resource (towns, keywordss, min_amount, saldo)
values('Żywiec','event',null,null);

