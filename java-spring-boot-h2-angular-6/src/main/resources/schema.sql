create table campaign
(
	id integer not null AUTO_INCREMENT,
	name varchar(255) not null,
	keywords varchar(255) not null,
	bid_amount integer not null,
	fund integer not null,
	status boolean not null,
	town varchar(255),
	radius integer not null,
	primary key(id)
);

create table resource
(
	id integer not null AUTO_INCREMENT,
	towns varchar(255) not null,
	keywordss varchar(255) not null,
	min_amount integer,
	saldo integer,
	primary key(id)
);