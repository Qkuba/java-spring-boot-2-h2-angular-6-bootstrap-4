package exercise.v2.javaspringbooth2angular6;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class CampaignRepository {
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	class CampaignRowMapper implements RowMapper<Campaign> {
		@Override
		public Campaign mapRow(ResultSet rs, int rowNum) throws SQLException {
			Campaign campaign = new Campaign();
			campaign.setId(rs.getLong("id"));
			campaign.setName(rs.getString("name"));
			campaign.setKeywords(rs.getString("keywords"));
			campaign.setBid_Amount(rs.getLong("bid_amount"));
			campaign.setFund(rs.getLong("fund"));
			campaign.setStatus(rs.getBoolean("status"));
			campaign.setTown(rs.getString("town"));
			campaign.setRadius(rs.getLong("radius"));
			return campaign;
		}
	}

	public List<Campaign> findAll() {
		return jdbcTemplate.query("select * from campaign", new CampaignRowMapper());
	}
	
	public Campaign findById(long id ) {
		return jdbcTemplate.queryForObject("select * from campaign where id=?", new Object[] { id },
				new BeanPropertyRowMapper<Campaign>(Campaign.class));
	}
	
	public int deleteById(long id) {
		return jdbcTemplate.update("delete from campaign where id=?", new Object[] { id });
	}

	public int insert(Campaign campaign) {
		return jdbcTemplate.update("insert into campaign (name, keywords, bid_amount,"
				+ "fund, status, town, radius) " + "values(?, ?, ?, ?, ?, ?, ?)",
				new Object[] {campaign.getName(), campaign.getKeywords(), campaign.getBid_Amount(), 
						campaign.getFund(), campaign.getStatus(), campaign.getTown(), campaign.getRadius() });
	}

	public int update(Campaign campaign) {
		return jdbcTemplate.update("update campaign " + " set name = ?, keywords = ?, bid_amount = ?, fund = ?, status = ?, town = ?, radius = ?" + " where id = ?",
				new Object[] {campaign.getName(), campaign.getKeywords(), campaign.getBid_Amount(), campaign.getFund(),
						campaign.getStatus(), campaign.getTown(), campaign.getRadius(), campaign.getId() });
	}
}
