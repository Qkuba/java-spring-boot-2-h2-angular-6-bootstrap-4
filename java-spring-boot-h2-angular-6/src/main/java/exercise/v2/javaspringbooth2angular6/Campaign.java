package exercise.v2.javaspringbooth2angular6;


public class Campaign {
	
	private Long id;
	private String name;
	private String keywords;
	private Long bid_amount;
	private Long fund;
	private Boolean status;
	private String town;
	private Long radius;
	
	public Campaign() {
		super();
	}
	
	public Campaign(Long id, String name, String keywords, Long bid_amount,
			Long fund, Boolean status, String town, Long radius) {
		super();
		this.id = id;
		this.name = name;
		this.keywords = keywords;
		this.bid_amount = bid_amount;
		this.fund = fund;
		this.status = status;
		this.town = town;
		this.radius = radius;
	}
	
	public Campaign(String name, String keywords, Long bid_amount,
			Long fund, Boolean status, String town, Long radius) {
		super();
		this.name = name;
		this.keywords = keywords;
		this.bid_amount = bid_amount;
		this.fund = fund;
		this.status = status;
		this.town = town;
		this.radius = radius;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	
	public Long getBid_Amount() {
		return bid_amount;
	}

	public void setBid_Amount(Long bid_amount) {
		this.bid_amount = bid_amount;
	}
	
	public Long getFund() {
		return fund;
	}

	public void setFund(Long fund) {
		this.fund = fund;
	}
	
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}
	
	public Long getRadius() {
		return radius;
	}

	public void setRadius(Long radius) {
		this.radius = radius;
	}
	
	@Override
	public String toString() {
		return String.format("Campaign [id=%s, name=%s, keywords=%s,bid_amount=%s, "
				+ "fund=%s, status=%s, town=%s, radius=%s]", id, name, 
				keywords, bid_amount, fund, status, town, radius);
	}
}

