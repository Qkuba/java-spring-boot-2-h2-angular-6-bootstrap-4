package exercise.v2.javaspringbooth2angular6;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResourceController {
	
	@Autowired
	ResourceRepository resourceRepository;
	
	@RequestMapping(method=RequestMethod.GET, value="/resource")
	public Iterable<Resource> resource() {
		return resourceRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/resource/{id}")
    public Resource show(@PathVariable Long id) {
        return resourceRepository.findById(id);
	}
}
