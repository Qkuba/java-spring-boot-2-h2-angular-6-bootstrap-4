package exercise.v2.javaspringbooth2angular6;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CampaignController {
	
	@Autowired
	CampaignRepository campaignRepository;
	@Autowired
	ResourceRepository resourceRepository;
	
	@RequestMapping(method=RequestMethod.GET, value="/campaign")
    public Iterable<Campaign> campaign() {
        return campaignRepository.findAll();
    }
	
	@RequestMapping(method=RequestMethod.POST, value="/campaign")
    public Campaign save(@RequestBody Campaign campaign) {
		campaignRepository.insert(campaign);
		resourceRepository.updateSaldo(campaign.getFund());
        return campaign;
    }

    @RequestMapping(method=RequestMethod.GET, value="/campaign-detail/{id}")
    public Campaign show(@PathVariable Long id) {
        return campaignRepository.findById(id);
    }
    
    @RequestMapping(method=RequestMethod.PUT, value="/campaign-detail/{id}")
    public Campaign update(@PathVariable Long id, @RequestBody Campaign campaign) {
    	Campaign c = campaignRepository.findById(id);
        if(campaign.getName() != null)
            c.setName(campaign.getName());
        if(campaign.getKeywords() != null)
            c.setKeywords(campaign.getKeywords());
        if(campaign.getBid_Amount() != null)
            c.setBid_Amount(campaign.getBid_Amount());
        if(campaign.getFund() != null)
            c.setFund(campaign.getFund());
        if(campaign.getStatus() != null)
            c.setStatus(campaign.getStatus());
        if(campaign.getTown() != null)
            c.setTown(campaign.getTown());
        if(campaign.getRadius() != null)
            c.setRadius(campaign.getRadius());
        campaignRepository.update(c);
        return campaign;
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/campaign-detail/{id}")
    public String delete(@PathVariable Long id) {
    	campaignRepository.deleteById(id);

        return "";
    }
}
