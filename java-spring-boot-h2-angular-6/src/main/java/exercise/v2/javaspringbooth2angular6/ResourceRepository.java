package exercise.v2.javaspringbooth2angular6;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class ResourceRepository {
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	class ResourceRowMapper implements RowMapper<Resource> {
		@Override
		public Resource mapRow(ResultSet rs, int rowNum) throws SQLException {
			Resource resource = new Resource();
			resource.setId(rs.getLong("id"));
			resource.setTowns(rs.getString("towns"));
			resource.setKeywordss(rs.getString("keywordss"));
			resource.setMin_Amount(rs.getLong("min_amount"));
			resource.setSaldo(rs.getLong("saldo"));
			return resource;
		}
	}
	
	public List<Resource> findAll() {
		return jdbcTemplate.query("select * from resource", new ResourceRowMapper());
	}
	
	public Resource findById(long id) {
		return jdbcTemplate.queryForObject("select * from resource where id=?",  new Object[] { id },
				new BeanPropertyRowMapper<Resource>(Resource.class));
	}
	
	public int updateSaldo(long diff) {
		return jdbcTemplate.update("update resource set saldo = saldo - ? where id =1",
				new Object[] { diff });
	}
}
