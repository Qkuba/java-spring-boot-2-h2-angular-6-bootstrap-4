package exercise.v2.javaspringbooth2angular6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@SpringBootApplication
public class JavaSpringBootH2Angular6Application implements WebMvcConfigurer {
	
	@Override
	public void addCorsMappings (CorsRegistry registry) {
		registry.addMapping("/**")
		.allowedOrigins("http://localhost:4200")
		.allowedMethods("GET","POST","PUT","DELETE");
	}
	
	public static void main(String[] args) {
		SpringApplication.run(JavaSpringBootH2Angular6Application.class, args);
	}
}
