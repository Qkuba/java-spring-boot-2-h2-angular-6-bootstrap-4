package exercise.v2.javaspringbooth2angular6;

public class Resource {
	
	private Long id;
	private String towns;
	private String keywordss;
	private Long min_amount;
	private Long saldo;
	
	public Resource() {
		super();
	}

	public Resource(Long id, String towns, String keywordss, Long min_amount,
			Long saldo) {
		super();
		this.id = id;
		this.towns = towns;
		this.keywordss = keywordss;
		this.min_amount = min_amount;
		this.saldo = saldo;
	}
	
	public Resource(String towns, String keywordss, Long min_amount,
			Long saldo) {
		super();
		this.towns = towns;
		this.keywordss = keywordss;
		this.min_amount = min_amount;
		this.saldo = saldo;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTowns() {
		return towns;
	}

	public void setTowns(String towns) {
		this.towns = towns;
	}

	public String getKeywordss() {
		return keywordss;
	}

	public void setKeywordss(String keywordss) {
		this.keywordss = keywordss;
	}
	
	public Long getMin_Amount() {
		return min_amount;
	}

	public void setMin_Amount(Long min_amount) {
		this.min_amount = min_amount;
	}
	
	public Long getSaldo() {
		return saldo;
	}

	public void setSaldo(Long saldo) {
		this.saldo = saldo;
	}
	
	@Override
	public String toString() {
		return String.format("Resource [id=%s, towns=%s, keywordss=%s,min_amount=%s, "
				+ "saldo=%s]", id, towns, keywordss, min_amount, saldo);
	}
}
