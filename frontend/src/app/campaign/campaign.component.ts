import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Campaign } from '../campaign';

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.css']
})
export class CampaignComponent implements OnInit {
  campaigns: Campaign[];
  resources: Resource[];
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get<Campaign[]>('http://localhost:8080/campaign').subscribe(data => {
      this.campaigns = data;
    });
    this.http.get<Resource[]>('http://localhost:8080/resource').subscribe(data2 => {
      this.resources = data2;
    });
  }
}
