import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Campaign } from '../campaign';

@Component({
  selector: 'app-campaign-detail',
  templateUrl: './campaign-detail.component.html',
  styleUrls: ['./campaign-detail.component.css']
})
export class CampaignDetailComponent implements OnInit {

  campaign: Campaign = new Campaign;

  constructor(private router: Router, private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    this.getCampaignDetail(this.route.snapshot.params['id']);
  }

  getCampaignDetail(id) {
    this.http.get<Campaign>('http://localhost:8080/campaign-detail/' + id).subscribe(data => {
      this.campaign = data;
    });
  }

  deleteCampaign(id) {
    this.http.delete('http://localhost:8080/campaign-detail/' + id)
      .subscribe(res => {
          this.router.navigate(['/campaign']);
        }, (err) => {
          console.log(err);
        }
      );
  }
}
