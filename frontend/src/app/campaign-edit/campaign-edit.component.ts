import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Campaign } from '../campaign';

const keywords = ['recycling', 'segregation', 'mpk', 'free ride', 'concert', 'event'];

@Component({
  selector: 'app-campaign-edit',
  templateUrl: './campaign-edit.component.html',
  styleUrls: ['./campaign-edit.component.css']
})
export class CampaignEditComponent implements OnInit {

  campaign: Campaign = new Campaign;

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 1 ? []
        : keywords.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )


  constructor(private router: Router, private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    this.getCampaign(this.route.snapshot.params['id']);
  }

  getCampaign(id) {
    this.http.get<Campaign>('http://localhost:8080/campaign-detail/' + id).subscribe(data => {
      this.campaign = data;
    });
  }

  updateCampaign(id) {
    this.http.put('http://localhost:8080/campaign-detail/' + id, this.campaign)
      .subscribe(res => {
          id = res['id'];
          this.router.navigate(['/campaign-detail', id]);
        }, (err) => {
          console.log(err);
        }
      );
  }
}
