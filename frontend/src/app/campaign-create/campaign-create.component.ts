import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { Campaign } from '../campaign';

const keywords = ['recycling', 'segregation', 'mpk', 'free ride', 'concert', 'event'];

@Component({
  selector: 'app-campaign-create',
  templateUrl: './campaign-create.component.html',
  styleUrls: ['./campaign-create.component.css']
})
export class CampaignCreateComponent implements OnInit {
  @ViewChild('campaignForm') campaignForm: NgForm;
  campaign: Campaign = new Campaign();
  resource: Resource[];

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 1 ? []
        : keywords.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.http.get<Resource[]>('http://localhost:8080/resource').subscribe(data => {
      this.resource = data;
    });
    this.campaign.bid_Amount = 1000;
  }

  saveCampaign() {
    if (this.campaignForm.valid) {
    this.http.post('http://localhost:8080/campaign', this.campaign)
      .subscribe(res => {
          this.router.navigate(['/campaign']);
        }, (err) => {
          console.log(err);
        }
      );
    } else {
      console.log('Nie przeszła walidacja');
    }
  }
}
