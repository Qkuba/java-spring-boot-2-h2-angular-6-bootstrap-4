export class Campaign {
    id: number | null | undefined;
    name: string;
    keywords: string;
    bid_Amount: number;
    fund: number;
    status: boolean;
    town: string;
    radius: number;
  }
