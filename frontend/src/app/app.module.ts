import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { CampaignComponent } from './campaign/campaign.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { CampaignDetailComponent } from './campaign-detail/campaign-detail.component';
import { CampaignCreateComponent } from './campaign-create/campaign-create.component';
import { CampaignEditComponent } from './campaign-edit/campaign-edit.component';

const appRoutes: Routes = [
  {
    path: 'campaign',
    component: CampaignComponent,
    data: { title: 'Campaigns List' }
  },
  {
    path: 'campaign-detail/:id',
    component: CampaignDetailComponent,
    data: { title: 'Campaign Details' }
  },
  {
    path: 'campaign-create',
    component: CampaignCreateComponent,
    data: { title: 'Create Campaign' }
  },
  {
    path: 'campaign-edit/:id',
    component: CampaignEditComponent,
    data: { title: 'Edit Campaign' }
  },
  { path: '',
    redirectTo: '/campaign',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    CampaignComponent,
    CampaignDetailComponent,
    CampaignCreateComponent,
    CampaignEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
